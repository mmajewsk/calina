
import setuptools.command.build_py

reqs = [
    "numpy",
    "pandas",
    "tqdm",
]



setuptools.setup(
    install_requires=reqs,
    name='calina_dataset',
    version='0.1',
    author="Maciej Majewski",
    author_email="mmajewsk@cern.ch",
    description="Dataset wrapper for the VELO run 1 and 2 calibration data",
    url="https://gitlab.com/mmajewsk/calina",
    package_dir={'calina_dataset': '.'},
    py_modules=['calibration_dataset', "processing"],
    package_data={'': ['module_mapping.csv']},
    include_package_data=True,
    packages=['calina_dataset'],
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ]

)
